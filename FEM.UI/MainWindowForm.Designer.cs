﻿namespace FEM.UI
{
    partial class MainWindowForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.btnSmooth = new System.Windows.Forms.Button();
            this.btnTriangulate = new System.Windows.Forms.Button();
            this.meshControlView = new FEM.UI.Views.MeshControlView();
            this.statisticView = new FEM.UI.Views.StatisticView();
            this.renderControl = new MeshRenderer.Core.GDI.RenderControl();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.btnSolve = new System.Windows.Forms.Button();
            this.femProblemView = new FEM.UI.Views.FemProblemView();
            this.renderer = new nzy3D.Plot3D.Rendering.View.Renderer3D();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Multiline = true;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.Padding = new System.Drawing.Point(0, 0);
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(850, 525);
            this.tabControl1.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.DimGray;
            this.tabPage1.Controls.Add(this.splitContainer1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(842, 499);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Triangulation";
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.DimGray;
            this.tabPage2.Controls.Add(this.splitContainer2);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(842, 499);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "FEM";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(3, 3);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.btnSmooth);
            this.splitContainer1.Panel1.Controls.Add(this.btnTriangulate);
            this.splitContainer1.Panel1.Controls.Add(this.meshControlView);
            this.splitContainer1.Panel1.Controls.Add(this.statisticView);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.renderControl);
            this.splitContainer1.Size = new System.Drawing.Size(836, 493);
            this.splitContainer1.SplitterDistance = 278;
            this.splitContainer1.TabIndex = 0;
            // 
            // btnSmooth
            // 
            this.btnSmooth.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSmooth.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnSmooth.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(169)))), ((int)(((byte)(244)))));
            this.btnSmooth.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(169)))), ((int)(((byte)(244)))));
            this.btnSmooth.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSmooth.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnSmooth.Location = new System.Drawing.Point(158, 465);
            this.btnSmooth.Name = "btnSmooth";
            this.btnSmooth.Size = new System.Drawing.Size(117, 23);
            this.btnSmooth.TabIndex = 2;
            this.btnSmooth.Text = "Load mesh";
            this.btnSmooth.UseVisualStyleBackColor = false;
            this.btnSmooth.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // btnTriangulate
            // 
            this.btnTriangulate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnTriangulate.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnTriangulate.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(169)))), ((int)(((byte)(244)))));
            this.btnTriangulate.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(169)))), ((int)(((byte)(244)))));
            this.btnTriangulate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTriangulate.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnTriangulate.Location = new System.Drawing.Point(5, 465);
            this.btnTriangulate.Name = "btnTriangulate";
            this.btnTriangulate.Size = new System.Drawing.Size(117, 23);
            this.btnTriangulate.TabIndex = 1;
            this.btnTriangulate.Text = "Triangulate";
            this.btnTriangulate.UseVisualStyleBackColor = false;
            this.btnTriangulate.Click += new System.EventHandler(this.btnTriangulate_Click);
            // 
            // meshControlView
            // 
            this.meshControlView.AutoScroll = true;
            this.meshControlView.AutoSize = true;
            this.meshControlView.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(114)))), ((int)(((byte)(114)))), ((int)(((byte)(114)))));
            this.meshControlView.Dock = System.Windows.Forms.DockStyle.Top;
            this.meshControlView.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.meshControlView.ForeColor = System.Drawing.Color.DarkGray;
            this.meshControlView.Location = new System.Drawing.Point(0, 0);
            this.meshControlView.Margin = new System.Windows.Forms.Padding(0);
            this.meshControlView.Name = "meshControlView";
            this.meshControlView.Size = new System.Drawing.Size(278, 156);
            this.meshControlView.TabIndex = 0;
            // 
            // statisticView
            // 
            this.statisticView.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(114)))), ((int)(((byte)(114)))), ((int)(((byte)(114)))));
            this.statisticView.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.statisticView.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.statisticView.ForeColor = System.Drawing.Color.DarkGray;
            this.statisticView.Location = new System.Drawing.Point(0, 159);
            this.statisticView.Name = "statisticView";
            this.statisticView.Size = new System.Drawing.Size(278, 334);
            this.statisticView.TabIndex = 3;
            // 
            // renderControl
            // 
            this.renderControl.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(33)))), ((int)(((byte)(33)))));
            this.renderControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.renderControl.ForeColor = System.Drawing.Color.White;
            this.renderControl.Location = new System.Drawing.Point(0, 0);
            this.renderControl.Name = "renderControl";
            this.renderControl.ShowRegions = true;
            this.renderControl.ShowVoronoi = false;
            this.renderControl.Size = new System.Drawing.Size(554, 493);
            this.renderControl.TabIndex = 0;
            this.renderControl.Text = "renderControl1";
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(3, 3);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.btnSolve);
            this.splitContainer2.Panel1.Controls.Add(this.femProblemView);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.renderer);
            this.splitContainer2.Size = new System.Drawing.Size(836, 493);
            this.splitContainer2.SplitterDistance = 271;
            this.splitContainer2.TabIndex = 3;
            // 
            // btnSolve
            // 
            this.btnSolve.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSolve.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnSolve.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnSolve.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(169)))), ((int)(((byte)(244)))));
            this.btnSolve.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(169)))), ((int)(((byte)(244)))));
            this.btnSolve.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSolve.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnSolve.Location = new System.Drawing.Point(151, 465);
            this.btnSolve.Name = "btnSolve";
            this.btnSolve.Size = new System.Drawing.Size(117, 23);
            this.btnSolve.TabIndex = 2;
            this.btnSolve.Text = "Solve";
            this.btnSolve.UseVisualStyleBackColor = false;
            this.btnSolve.Click += new System.EventHandler(this.btnSolve_Click);
            // 
            // femProblemView
            // 
            this.femProblemView.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(114)))), ((int)(((byte)(114)))), ((int)(((byte)(114)))));
            this.femProblemView.Dock = System.Windows.Forms.DockStyle.Top;
            this.femProblemView.Location = new System.Drawing.Point(0, 0);
            this.femProblemView.Name = "femProblemView";
            this.femProblemView.Size = new System.Drawing.Size(271, 263);
            this.femProblemView.TabIndex = 1;
            // 
            // renderer
            // 
            this.renderer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(33)))), ((int)(((byte)(33)))));
            this.renderer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.renderer.Location = new System.Drawing.Point(0, 0);
            this.renderer.Name = "renderer";
            this.renderer.Size = new System.Drawing.Size(561, 493);
            this.renderer.TabIndex = 1;
            this.renderer.VSync = false;
            // 
            // MainWindowForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(850, 525);
            this.Controls.Add(this.tabControl1);
            this.Name = "MainWindowForm";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.MainWindowForm_Load);
            this.ResizeBegin += new System.EventHandler(this.ResizeBeginHandler);
            this.ResizeEnd += new System.EventHandler(this.ResizeEndHandler);
            this.Resize += new System.EventHandler(this.ResizeHandler);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private MeshRenderer.Core.GDI.RenderControl renderControl;
        private Views.MeshControlView meshControlView;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button btnSmooth;
        private System.Windows.Forms.Button btnTriangulate;
        private Views.StatisticView statisticView;
        private Views.FemProblemView femProblemView;
        private System.Windows.Forms.Button btnSolve;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private nzy3D.Plot3D.Rendering.View.Renderer3D renderer;
    }
}

