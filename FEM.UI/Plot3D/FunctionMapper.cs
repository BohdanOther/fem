﻿using System;
using FEM.Core;
using nzy3D.Plot3D.Builder;

namespace FEM.UI.Plot3D
{
    class FunctionMapper : Mapper
    {
        private Func<double, double, double> _func;

        public FunctionMapper(FEMSolution solution)
        {
            _func = solution.GetValue;
        }

        public FunctionMapper(Func<double, double, double> func)
        {
            _func = func;
        }

        public override double f(double x, double y)
        {
            return _func(x, y);
        }
    }
}

