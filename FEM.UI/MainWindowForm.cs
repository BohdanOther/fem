﻿using FEM.Core;
using FEM.Core.Matrixes;
using FEM.UI.IO;
using MeshRenderer.Core;
using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using FEM.Common.Functions;
using FEM.UI.Plot3D;
using nzy3D.Chart;
using nzy3D.Chart.Controllers.Thread.Camera;
using nzy3D.Colors;
using nzy3D.Colors.ColorMaps;
using nzy3D.Plot3D.Builder;
using nzy3D.Plot3D.Builder.Concrete;
using nzy3D.Plot3D.Primitives;
using nzy3D.Plot3D.Primitives.Axes.Layout;
using nzy3D.Plot3D.Rendering.Canvas;
using TriangleNet;
using TriangleNet.Geometry;
using Color = System.Drawing.Color;

namespace FEM.UI
{
    public partial class MainWindowForm : Form
    {
        Settings settings;
        InputGeometry input;
        Mesh mesh;

        RenderManager renderManager;
        RenderData renderData;

        FEMSolution solution;

        public MainWindowForm()
        {
            InitializeComponent();
        }

        #region Plotting

        private CameraThreadController controller;
        private IAxeLayout axeLayout;

        private Chart chart;

        private Shape surface;

        private void InitRenderer(Mapper f, double maxX, double maxY)
        {
            // Create a range for the graph generation
            var rangex = new nzy3D.Maths.Range(0, maxX);
            var rangey = new nzy3D.Maths.Range(0, maxY);
            var steps = 50;

            // Build a nice surface to display with cool alpha colors 
            // (alpha 0.8 for surface color and 0.5 for wireframe)
            // solution here
            surface = Builder.buildOrthonomal(new OrthonormalGrid(rangex, steps, rangey, steps), f);

            surface.ColorMapper = new ColorMapper(new ColorMapRainbow(), surface.Bounds.zmin, surface.Bounds.zmax, new nzy3D.Colors.Color(1, 1, 1, 0.8));
            surface.FaceDisplayed = true;
            surface.WireframeDisplayed = true;
            surface.WireframeColor = nzy3D.Colors.Color.CYAN;
            surface.WireframeColor.mul(new nzy3D.Colors.Color(1, 1, 1, 0.5));
            // Create the chart and embed the surface within

            chart = new Chart(renderer, Quality.Nicest);
            chart.Scene.Graph.Add(surface);
            axeLayout = chart.AxeLayout;
            axeLayout.MainColor = nzy3D.Colors.Color.WHITE;
            chart.View.BackgroundColor = nzy3D.Colors.Color.GRAY;

            // Create a mouse control
            var mouse = new nzy3D.Chart.Controllers.Mouse.Camera.CameraMouseController();
            mouse.addControllerEventListener(renderer);
            chart.addController(mouse);

            // This is just to ensure code is reentrant (used when code is not called in Form_Load but another reentrant event)
            DisposeBackgroundThread();

            // Create a thread to control the camera based on mouse movements
            controller = new CameraThreadController();
            controller.addControllerEventListener(renderer);
            mouse.addSlaveThreadController(controller);
            chart.addController(controller);
            controller.Start();
            // Associate the chart with current control
            renderer.setView(chart.View);

            this.Refresh();
        }

        private void DisposeBackgroundThread()
        {
            controller?.Dispose();
        }

        #endregion


        #region Resize event handler

        bool isResizing = false;
        Size oldClientSize;

        private void ResizeHandler(object sender, EventArgs e)
        {
            if (this.ClientSize != this.oldClientSize)
            {
                this.oldClientSize = this.ClientSize;
                renderManager.HandleResize();
            }
            // Handle window minimize and maximize
            if (!isResizing)
            {
                renderManager.HandleResize();
            }
        }

        private void ResizeEndHandler(object sender, EventArgs e)
        {
            isResizing = false;

            if (this.ClientSize != this.oldClientSize)
            {
                this.oldClientSize = this.ClientSize;
                renderManager.HandleResize();
            }
        }

        private void ResizeBeginHandler(object sender, EventArgs e)
        {
            isResizing = true;
        }

        #endregion

        #region Mesh State Handlers

        private void HandleNewInput()
        {
            // Reset mesh
            mesh = null;

            // Reset state
            settings.RefineMode = false;
            settings.ExceptionThrown = false;

            //// Reset buttons
            btnTriangulate.Enabled = true;
            btnTriangulate.Text = "Triangulate";
            btnSmooth.Enabled = false;

            //// Update Statistic view
            statisticView.HandleNewInput(input);

            // Render input
            renderData.SetInputGeometry(input);
            renderManager.SetData(renderData);

            // Update window caption
            this.Text = "Triangle.NET - Mesh Explorer - " + settings.CurrentFile;
        }

        private void HandleMeshUpdate()
        {
            // Render mesh
            renderData.SetMesh(mesh);
            renderManager.SetData(renderData);

            // Update Statistic view
            statisticView.HandleMeshUpdate(mesh);

            HandleMeshChange();
        }

        private void HandleMeshChange()
        {
            //// Update Statistic view
            statisticView.HandleMeshChange(mesh);
        }

        private void HandleMeshImport()
        {
            // Render mesh
            renderData.SetMesh(mesh);
            renderManager.SetData(renderData);

            // Update window caption
            this.Text = "Mesh Explorer - " + settings.CurrentFile;

            // Update Statistic view
            statisticView.HandleMeshImport(input, mesh);

            // Set refine mode
            btnTriangulate.Enabled = true;
            btnTriangulate.Text = "Refine";

            settings.RefineMode = true;

            HandleMeshChange();
        }

        #endregion

        #region Commands

        private void TriangulateOrRefine()
        {
            if ((input == null && !settings.RefineMode) || settings.ExceptionThrown)
            {
                return;
            }

            if (settings.RefineMode == false)
            {
                Triangulate();

                if (meshControlView.ParamQualityChecked)
                {
                    btnTriangulate.Text = "Refine";
                    btnSmooth.Enabled = mesh.IsPolygon;
                }
            }
            else if (meshControlView.ParamQualityChecked)
            {
                Refine();
            }
        }

        private void Triangulate()
        {
            if (input == null) return;

            mesh = new Mesh();

            // mesh.Behavior.ConformingDelaunay = true;
            // mesh.Behavior.Convex = false;
            // mesh.Behavior.Algorithm = TriangulationAlgorithm.SweepLine;

            if (meshControlView.ParamQualityChecked)
            {
                mesh.Behavior.Quality = true;

                mesh.Behavior.MinAngle = meshControlView.ParamMinAngleValue;

                double maxAngle = meshControlView.ParamMaxAngleValue;

                if (maxAngle < 180)
                {
                    mesh.Behavior.MaxAngle = maxAngle;
                }
            }

            try
            {
                mesh.Triangulate(input);

                statisticView.UpdateStatistic(mesh);

                HandleMeshUpdate();

                if (meshControlView.ParamQualityChecked)
                {
                    settings.RefineMode = true;
                }
            }
            catch (Exception ex)
            {
                //LockOnException();
                MessageBox.Show("Exception - Triangulate", ex.Message, MessageBoxButtons.OK);
            }
        }

        private void Refine()
        {
            if (mesh == null) return;


            double area = meshControlView.ParamMaxAreaValue;

            if (area > 0 && area < 1)
            {
                mesh.Behavior.MaxArea = area * statisticView.Statistic.LargestArea;
            }

            mesh.Behavior.MinAngle = meshControlView.ParamMinAngleValue;

            double maxAngle = meshControlView.ParamMaxAngleValue;

            if (maxAngle < 180)
            {
                mesh.Behavior.MaxAngle = maxAngle;
            }

            try
            {
                mesh.Refine();

                statisticView.UpdateStatistic(mesh);

                HandleMeshUpdate();
            }
            catch (Exception ex)
            {
                //  LockOnException();
                MessageBox.Show("Exception - Refine", ex.Message, MessageBoxButtons.OK);
            }
        }

        private void Smooth()
        {
            if (mesh == null || settings.ExceptionThrown) return;

            if (!mesh.IsPolygon)
            {
                return;
            }

            try
            {
                mesh.Smooth();

                statisticView.UpdateStatistic(mesh);

                HandleMeshUpdate();
            }
            catch (Exception ex)
            {
                // LockOnException();
                MessageBox.Show("Exception - Smooth", ex.Message, MessageBoxButtons.OK);
            }
        }

        private void Reload()
        {
            if (input != null)
            {
                mesh = null;
                settings.RefineMode = false;
                settings.ExceptionThrown = false;

                HandleNewInput();
            }
        }

        private void OpenWithDialog()
        {
            OpenFileDialog ofd = new OpenFileDialog();

            ofd.Filter = settings.OfdFilter;
            ofd.FilterIndex = settings.OfdFilterIndex;
            ofd.InitialDirectory = settings.OfdDirectory;
            ofd.FileName = "";

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                if (Open(ofd.FileName))
                {
                    // Update folder settings
                    settings.OfdFilterIndex = ofd.FilterIndex;
                    settings.OfdDirectory = Path.GetDirectoryName(ofd.FileName);
                }
            }
        }

        private bool Open(string filename)
        {
            if (FileProcessor.ContainsMeshData(filename))
            {
                if (MessageBox.Show("Import mesh", Settings.ImportString, MessageBoxButtons.YesNo) == DialogResult.OK)
                {
                    input = null;
                    mesh = FileProcessor.Import(filename);

                    if (mesh != null)
                    {
                        statisticView.UpdateStatistic(mesh);

                        // Update settings
                        settings.CurrentFile = Path.GetFileName(filename);

                        HandleMeshImport();
                        btnSmooth.Enabled = true; // TODO: Remove
                    }

                    return true;
                }
            }

            input = FileProcessor.Read(filename);

            if (input != null)
            {
                // Update settings
                settings.CurrentFile = Path.GetFileName(filename);

                HandleNewInput();
            }

            return true;
        }

        #endregion

        #region Form Events

        private void MainWindowForm_Load(object sender, EventArgs e)
        {
            oldClientSize = this.ClientSize;
            settings = new Settings();

            renderManager = new RenderManager { RenderControl = renderControl };
            renderManager.Initialize();
            renderData = new RenderData();
        }

        private void btnTriangulate_Click(object sender, EventArgs e)
        {
            TriangulateOrRefine();
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            OpenWithDialog();
        }

        protected override void OnMouseWheel(MouseEventArgs e)
        {
            var container = this.splitContainer1.Panel2.ClientRectangle;

            var pt = e.Location;
            pt.Offset(-splitContainer1.SplitterDistance, 0);

            if (container.Contains(pt))
            {
                renderManager.Zoom(((float)pt.X) / container.Width,
                    ((float)pt.Y) / container.Height, e.Delta);
            }
            base.OnMouseWheel(e);
        }

        #endregion

        private void btnSolve_Click(object sender, EventArgs e)
        {
            if (mesh == null)
            {
                MessageBox.Show("Mesh is empty");
                return;
            }

            var problem = femProblemView.GetProblem();

            if (problem != null)
            {
                problem.Mesh = mesh;

                var solver = new FEMSolver();
                solution = solver.Solve(problem);

                FunctionMapper mapper = new FunctionMapper(solution);

                InitRenderer(mapper, 2, 1);
            }
        }

    }
}