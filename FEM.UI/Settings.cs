﻿// -----------------------------------------------------------------------
// <copyright file="Settings.cs" company="">
// Christian Woltering, Triangle.NET, http://triangle.codeplex.com/
// </copyright>
// -----------------------------------------------------------------------

using System.IO;
using System.Windows.Forms;

namespace FEM.UI
{
    /// <summary>
    /// Stores some of the data used in the main application.
    /// </summary>
    public class Settings
    {
        // String resources
        public static string ImportString = "The selected file has associated mesh information. " +
            "You can choose to import the mesh or just read the geometry.";

        // Open file dialog
        public string OfdDirectory { get; set; }
        public string OfdFilter { get; set; }
        public int OfdFilterIndex { get; set; }

        // Save file dialog
        public string SfdDirectory { get; set; }
        public string SfdFilter { get; set; }
        public int SfdFilterIndex { get; set; }

        public string CurrentFile { get; set; }

        public bool RefineMode { get; set; }
        public bool ExceptionThrown { get; set; }

        public Settings()
        {
            if (Directory.Exists(@"..\..\..\Data\"))
            {
                OfdDirectory = Path.GetFullPath(@"..\..\..\Data\");
            }
            else if (Directory.Exists(@"Data\"))
            {
                OfdDirectory = Path.GetFullPath(@"Data\");
            }
            else
            {
                OfdDirectory = Application.StartupPath;
            }

            SfdDirectory = OfdDirectory;
            SfdFilter = "Data file (*.dat)|*.dat";
            SfdFilter += "|Polygon file (*.node;*.poly)|*.node;*.poly";
            SfdFilter += "|Polygon JSON (*.json)|*.json";
            SfdFilterIndex = 1;

            OfdFilter = SfdFilter;

            OfdFilterIndex = 1;

            CurrentFile = "";

            RefineMode = false;
            ExceptionThrown = false;
        }
    }
}
