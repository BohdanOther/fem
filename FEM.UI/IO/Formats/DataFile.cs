﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using TriangleNet;
using TriangleNet.Geometry;
using TriangleNet.IO;

namespace FEM.UI.IO.Formats
{
    public class DataFile : IMeshFile
    {
        TriangleFormat format = new TriangleFormat();

        public string[] Extensions => new[] { ".dat" };

        public bool ContainsMeshData(string filename) => false;

        public InputGeometry Read(string filename)
        {
            var pts = (
                from line in File.ReadAllLines(filename)
                select line.Trim()
                       into _line
                where !string.IsNullOrEmpty(_line)
                select _line.Split(new[] { ' ', ',' }, StringSplitOptions.RemoveEmptyEntries)
                       into xy
                let x = double.Parse(xy[0])
                let y = double.Parse(xy[1])
                select new Point(x, y)
                ).ToList();

            var builder = new StringBuilder();

            builder.AppendLine($"{pts.Count} {2} {0} {0}");

            for (var i = 0; i < pts.Count; i++)
            {
                builder.AppendLine($"{i + 1} {pts[i].X} {pts[i].Y}");
            }

            builder.AppendLine($"{pts.Count}");

            for (var i = 0; i < pts.Count - 1; i++)
            {
                builder.AppendLine($"{i + 1} {i + 1} {i + 2}");
            }
            builder.AppendLine($"{pts.Count} {pts.Count } {1}");
            builder.AppendLine($"{0}");

            using (var write = new StreamWriter("temp.poly"))
            {
                write.Write(builder.ToString());
            }


            return FileReader.ReadPolyFile("temp.poly");
        }

        public Mesh Import(string filename)
        {
            return new Mesh();
        }

        public void Write(Mesh mesh, string filename)
        {
            throw new NotImplementedException();
        }
    }
}
