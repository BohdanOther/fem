﻿using System;
using System.Windows.Forms;
using FEM.Common.Functions;
using FEM.Core;

namespace FEM.UI.Views
{
    public partial class FemProblemView : UserControl
    {
        public FemProblemView()
        {
            InitializeComponent();
        }

        public FEMProblem GetProblem()
        {
            try
            {
                var strF = tbF.Text;
                var strUc = tbUc.Text;

                var alpha = double.Parse(tbAlpha.Text);
                var beta = double.Parse(tbBeta.Text);
                var c = double.Parse(tbC.Text);

                return new FEMProblem
                {
                    EnvironmentFunc = new SymbolicFunction(strUc).Val,
                    GivenFunc = new SymbolicFunction(strF).Val,
                    C = c,
                    Alpha = alpha,
                    Beta = beta.Equals(0.0) ? 0.00001 : beta
                };
            }
            catch (Exception)
            {

                MessageBox.Show("Input error");
                return null;
            }

        }

        private void btnSolve_Click(object sender, EventArgs e)
        {
            tbF.Text = "0.0";
            tbUc.Text = "50.0";

            tbAlpha.Text = "1";
            tbBeta.Text = "1";
            tbC.Text = "0";
        }
    }
}
