﻿using TriangleNet;
using TriangleNet.Geometry;

namespace FEM.UI.Views
{
    public interface IView
    {
        void HandleNewInput(InputGeometry geometry);
        void HandleMeshImport(InputGeometry geometry, Mesh mesh);
        void HandleMeshUpdate(Mesh mesh);
        void HandleMeshChange(Mesh mesh);
    }
}
