﻿using System.Linq;
using System.Windows.Forms;
using TriangleNet;
using TriangleNet.Geometry;
using TriangleNet.Tools;

namespace FEM.UI.Views
{
    public partial class StatisticView : UserControl, IView
    {
        Statistic statistic = new Statistic();
        QualityMeasure quality;

        public Statistic Statistic
        {
            get { return statistic; }
        }

        public StatisticView()
        {
            InitializeComponent();
        }

        public void UpdateStatistic(Mesh mesh)
        {
            statistic.Update(mesh, 10);
        }

        #region IView

        public void HandleNewInput(InputGeometry geometry)
        {
            // Reset labels
            lbNumVert2.Text = "-";
            lbNumTri2.Text = "-";
            lbNumSeg2.Text = "-";

            lbNumVert.Text = geometry.Count.ToString();
            lbNumSeg.Text = geometry.Segments.Count().ToString();
            lbNumTri.Text = "0";

            // Statistics labels
            lbAreaMin.Text = "-";
            lbAreaMax.Text = "-";
            lbEdgeMin.Text = "-";
            lbEdgeMax.Text = "-";
            lbAngleMin.Text = "-";
            lbAngleMax.Text = "-";

            // Quality labels
            lbQualAlphaMin.Text = "-";
            lbQualAlphaAve.Text = "-";
            lbQualAspectMin.Text = "-";
            lbQualAspectAve.Text = "-";
        }

        public void HandleMeshImport(InputGeometry geometry, Mesh mesh)
        {
            // Previous mesh stats
            lbNumVert2.Text = "-";
            lbNumTri2.Text = "-";
            lbNumSeg2.Text = "-";
        }

        public void HandleMeshUpdate(Mesh mesh)
        {
            // Previous mesh stats
            lbNumVert2.Text = lbNumVert.Text;
            lbNumTri2.Text = lbNumTri.Text;
            lbNumSeg2.Text = lbNumSeg.Text;
        }

        public void HandleMeshChange(Mesh mesh)
        {
            // New mesh stats
            lbNumVert.Text = statistic.Vertices.ToString();
            lbNumSeg.Text = statistic.ConstrainedEdges.ToString();
            lbNumTri.Text = statistic.Triangles.ToString();


            lbAreaMin.Text = Utils.DoubleToString(statistic.SmallestArea);
            lbAreaMax.Text = Utils.DoubleToString(statistic.LargestArea);
            lbEdgeMin.Text = Utils.DoubleToString(statistic.ShortestEdge);
            lbEdgeMax.Text = Utils.DoubleToString(statistic.LongestEdge);
            lbAngleMin.Text = Utils.AngleToString(statistic.SmallestAngle);
            lbAngleMax.Text = Utils.AngleToString(statistic.LargestAngle);

            // Update quality
            if (quality == null)
            {
                quality = new QualityMeasure();
            }

            quality.Update(mesh);

            lbQualAlphaMin.Text = Utils.DoubleToString(quality.AlphaMinimum);
            lbQualAlphaAve.Text = Utils.DoubleToString(quality.AlphaAverage);

            lbQualAspectMin.Text = Utils.DoubleToString(quality.Q_Minimum);
            lbQualAspectAve.Text = Utils.DoubleToString(quality.Q_Average);
        }

        #endregion
    }
}
