﻿using System.Windows.Forms;

namespace FEM.UI.Views
{
    partial class MeshControlView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbMaxArea = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lbMinAngle = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lbMaxAngle = new System.Windows.Forms.Label();
            this.slMaxAngle = new System.Windows.Forms.TrackBar();
            this.slMaxArea = new System.Windows.Forms.TrackBar();
            this.slMinAngle = new System.Windows.Forms.TrackBar();
            this.cbQuality = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.slMaxAngle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.slMaxArea)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.slMinAngle)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbMaxArea
            // 
            this.lbMaxArea.AutoSize = true;
            this.lbMaxArea.ForeColor = System.Drawing.Color.White;
            this.lbMaxArea.Location = new System.Drawing.Point(231, 85);
            this.lbMaxArea.Name = "lbMaxArea";
            this.lbMaxArea.Size = new System.Drawing.Size(13, 13);
            this.lbMaxArea.TabIndex = 20;
            this.lbMaxArea.Text = "0";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(12, 85);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(81, 13);
            this.label6.TabIndex = 23;
            this.label6.Text = "Maximum area";
            // 
            // lbMinAngle
            // 
            this.lbMinAngle.AutoSize = true;
            this.lbMinAngle.ForeColor = System.Drawing.Color.White;
            this.lbMinAngle.Location = new System.Drawing.Point(231, 30);
            this.lbMinAngle.Name = "lbMinAngle";
            this.lbMinAngle.Size = new System.Drawing.Size(19, 13);
            this.lbMinAngle.TabIndex = 22;
            this.lbMinAngle.Text = "20";
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(114)))), ((int)(((byte)(114)))), ((int)(((byte)(114)))));
            this.label8.ForeColor = System.Drawing.Color.Gainsboro;
            this.label8.Location = new System.Drawing.Point(12, 113);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(238, 33);
            this.label8.TabIndex = 25;
            this.label8.Text = "Hint: maximum area values of 0 or 1 will be irgnored (no area constraints are set" +
    ").";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(12, 30);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(87, 13);
            this.label5.TabIndex = 21;
            this.label5.Text = "Minimum angle";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(12, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 13);
            this.label2.TabIndex = 21;
            this.label2.Text = "Maximum angle";
            // 
            // lbMaxAngle
            // 
            this.lbMaxAngle.AutoSize = true;
            this.lbMaxAngle.ForeColor = System.Drawing.Color.White;
            this.lbMaxAngle.Location = new System.Drawing.Point(230, 56);
            this.lbMaxAngle.Name = "lbMaxAngle";
            this.lbMaxAngle.Size = new System.Drawing.Size(25, 13);
            this.lbMaxAngle.TabIndex = 22;
            this.lbMaxAngle.Text = "180";
            // 
            // slMaxAngle
            // 
            this.slMaxAngle.Location = new System.Drawing.Point(106, 53);
            this.slMaxAngle.Maximum = 100;
            this.slMaxAngle.Name = "slMaxAngle";
            this.slMaxAngle.Size = new System.Drawing.Size(119, 45);
            this.slMaxAngle.TabIndex = 18;
            this.slMaxAngle.Text = "darkSlider1";
            this.slMaxAngle.TickStyle = System.Windows.Forms.TickStyle.None;
            this.slMaxAngle.ValueChanged += new System.EventHandler(this.slMaxAngle_ValueChanging);
            // 
            // slMaxArea
            // 
            this.slMaxArea.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(114)))), ((int)(((byte)(114)))), ((int)(((byte)(114)))));
            this.slMaxArea.Location = new System.Drawing.Point(105, 81);
            this.slMaxArea.Maximum = 100;
            this.slMaxArea.Name = "slMaxArea";
            this.slMaxArea.Size = new System.Drawing.Size(119, 45);
            this.slMaxArea.TabIndex = 19;
            this.slMaxArea.Text = "darkSlider1";
            this.slMaxArea.TickStyle = System.Windows.Forms.TickStyle.None;
            this.slMaxArea.ValueChanged += new System.EventHandler(this.slMaxArea_ValueChanging);
            // 
            // slMinAngle
            // 
            this.slMinAngle.Location = new System.Drawing.Point(105, 30);
            this.slMinAngle.Maximum = 100;
            this.slMinAngle.Name = "slMinAngle";
            this.slMinAngle.Size = new System.Drawing.Size(119, 45);
            this.slMinAngle.TabIndex = 18;
            this.slMinAngle.Text = "darkSlider1";
            this.slMinAngle.TickStyle = System.Windows.Forms.TickStyle.None;
            this.slMinAngle.Value = 50;
            this.slMinAngle.ValueChanged += new System.EventHandler(this.slMinAngle_ValueChanging);
            // 
            // cbQuality
            // 
            this.cbQuality.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(114)))), ((int)(((byte)(114)))), ((int)(((byte)(114)))));
            this.cbQuality.ForeColor = System.Drawing.Color.LightGray;
            this.cbQuality.Location = new System.Drawing.Point(15, 0);
            this.cbQuality.Name = "cbQuality";
            this.cbQuality.Size = new System.Drawing.Size(100, 17);
            this.cbQuality.TabIndex = 17;
            this.cbQuality.Text = "Quality mesh";
            this.cbQuality.UseVisualStyleBackColor = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.slMaxArea);
            this.groupBox1.Controls.Add(this.slMaxAngle);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.slMinAngle);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.lbMinAngle);
            this.groupBox1.Controls.Add(this.lbMaxAngle);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.cbQuality);
            this.groupBox1.Controls.Add(this.lbMaxArea);
            this.groupBox1.Location = new System.Drawing.Point(4, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(264, 149);
            this.groupBox1.TabIndex = 28;
            this.groupBox1.TabStop = false;
            // 
            // MeshControlView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(114)))), ((int)(((byte)(114)))), ((int)(((byte)(114)))));
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.DarkGray;
            this.Name = "MeshControlView";
            this.Size = new System.Drawing.Size(272, 159);
            ((System.ComponentModel.ISupportInitialize)(this.slMaxAngle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.slMaxArea)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.slMinAngle)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lbMaxArea;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lbMinAngle;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TrackBar slMaxArea;
        private System.Windows.Forms.TrackBar slMinAngle;
        private System.Windows.Forms.CheckBox cbQuality;
        private System.Windows.Forms.TrackBar slMaxAngle;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lbMaxAngle;
        private GroupBox groupBox1;
    }
}
