﻿using FEM.Core.FiniteElements;
using FEM.Core.Geometry;

namespace FEM.Core
{
    internal static class Utils
    {
        internal static double wedgeProduct(Point p11, Point p12, Point p21, Point p22)
        {
            return (p12.X - p11.X) * (p22.Y - p21.Y) - (p12.Y - p11.Y) * (p22.X - p21.X);
        }

        internal static double TriangleArea(Triangle t)
        {
            var arr = new[,]
         {
                {1.0, t[0].X, t[0].Y },
                {1.0, t[1].X, t[1].Y },
                {1.0, t[2].X, t[2].Y }
            };

            var matrix = MathNet.Numerics.LinearAlgebra.Matrix<double>.Build.DenseOfArray(arr);
            return 0.5 * matrix.Determinant();
        }

        internal static Point ToPoint(this TriangleNet.Data.Vertex v)
        {
            return new Point(v.X, v.Y);
        }

        internal static FiniteElementTriangle ToFiniteElement(this TriangleNet.Data.Triangle tri)
        {
            return new FiniteElementTriangle(
                tri.GetVertex(0).ToPoint(),
                tri.GetVertex(1).ToPoint(),
                tri.GetVertex(2).ToPoint()
                );
        }

    }
}
