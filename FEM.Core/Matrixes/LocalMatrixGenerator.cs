﻿using System;
using System.Collections.Generic;
using FEM.Core.FiniteElements;
using FEM.Core.Math;
using TriangleNet;

namespace FEM.Core.Matrixes
{
    public class LocalMatrixGenerator
    {
        public LocalMatrix LocalMatrix { get; private set; }
        public LocalVector LocalVector { get; private set; }

        private readonly GaussianQuadratureIntegrator integrator;
        private readonly List<FiniteElementTriangle> elements;
        private readonly FEMProblem problem;

        private readonly double alphaOverBeta;

        public LocalMatrixGenerator(GaussianQuadratureIntegrator integrator, List<FiniteElementTriangle> elements, FEMProblem problem)
        {
            this.integrator = integrator;
            this.elements = elements;
            this.problem = problem;

            alphaOverBeta = problem.Alpha/problem.Beta;
        }

        public void CalcMatrixes(int elemIndex)
        {
            var elem = elements.Find(el => el.Number == elemIndex);
            if (elem == null) return;

            var arr1 = GetMatrix1(elem);
            var arr2 = GetMatrix2(elem);
            //var arr3 = // don't compute for now
            var arr4 = GetMatrix4(elem);

            var vec5 = GetVector5(elem);
            var vec6 = GetVector6(elem);


            LocalMatrix = new LocalMatrix(3);
            LocalVector = new LocalVector(3);
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    var val = arr1[i, j] + arr2[i, j] + arr4[i, j];// + arr3[i,j]
                    LocalMatrix[i, j] = new LocalMatrix.Item(val, elem.Nodes[i].Number, elem.Nodes[j].Number);
                }

                var vecVal = vec5[i] + vec6[i];
                LocalVector[i] = new LocalVector.Item(vecVal, elem.Nodes[i].Number);
            }
        }

        private double[,] GetMatrix1(FiniteElementTriangle elem)
        {
            // D =  { x3 - x2, x1 - x3, x2 - x1 }
            //      { y3 - y2, y1 - y3, y2 - y1 }
            //
            // A = transponse(D) * D / 4*Area(D)
            // from: https://en.wikipedia.org/wiki/Stiffness_matrix

            var x1 = elem[0].X;
            var x2 = elem[1].X;
            var x3 = elem[2].X;

            var y1 = elem[0].Y;
            var y2 = elem[1].Y;
            var y3 = elem[2].Y;

            var s4 = elem.Square * 4.0;

            var arr = new double[3, 3];
            arr[0, 0] = (System.Math.Pow(x3 - x2, 2) + System.Math.Pow(y3 - y2, 2)) / s4;
            arr[0, 1] = ((x1 - x3) * (x3 - x2) + (y1 - y3) * (y3 - y2)) / s4;
            arr[0, 2] = ((x2 - x1) * (x3 - x2) + (y2 - y1) * (y3 - y2)) / s4;

            arr[1, 0] = ((x1 - x3) * (x3 - x2) + (y1 - y3) * (y3 - y2)) / s4;
            arr[1, 1] = (System.Math.Pow(x1 - x3, 2) + System.Math.Pow(y1 - y3, 2)) / s4;
            arr[1, 2] = ((x2 - x1) * (x1 - x3) + (y2 - y1) * (y1 - y3)) / s4;

            arr[2, 0] = ((x2 - x1) * (x3 - x2) + (y2 - y1) * (y3 - y2)) / s4;
            arr[2, 1] = ((x2 - x1) * (x1 - x3) + (y2 - y1) * (y1 - y3)) / s4;
            arr[2, 2] = (System.Math.Pow(x2 - x1, 2) + System.Math.Pow(y2 - y1, 2)) / s4;

            return arr;
        }

        private double[,] GetMatrix2(FiniteElementTriangle elem)
        {
            var res = new double[3, 3];

            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    Func<double, double, double> func = (x, y) => elem.Nodes[i].LocalFunction.getValue(x, y) * elem.Nodes[j].LocalFunction.getValue(x, y);

                    var AB = elem.Segment1.IsBoundary ? integrator.IntegrateOverLine(elem.Segment1, func) : 0.0;
                    var BC = elem.Segment2.IsBoundary ? integrator.IntegrateOverLine(elem.Segment2, func) : 0.0;
                    var CA = elem.Segment3.IsBoundary ? integrator.IntegrateOverLine(elem.Segment3, func) : 0.0;

                    res[i, j] = -alphaOverBeta * (AB + BC + CA);
                }
            }

            return res;
        }

        private double[,] GetMatrix4(FiniteElementTriangle elem)
        {
            var res = new double[3, 3];

            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    var coef = problem.C;
                    Func<double, double, double> func = (x, y) => elem.Nodes[i].LocalFunction.getValue(x, y) * elem.Nodes[j].LocalFunction.getValue(x, y);

                    res[i, j] = coef * integrator.IntegrateOverTriangleSurface(elem, func);
                }
            }

            return res;
        }

        private double[] GetVector5(FiniteElementTriangle elem)
        {
            var res = new double[3];

            for (int i = 0; i < 3; i++)
            {
                var coef = -alphaOverBeta;
                Func<double, double, double> func = (x, y) => elem.Nodes[i].LocalFunction.getValue(x, y) * problem.EnvironmentFunc(x, y);

                var AB = elem.Segment1.IsBoundary ? integrator.IntegrateOverLine(elem.Segment1, func) : 0.0;
                var BC = elem.Segment2.IsBoundary ? integrator.IntegrateOverLine(elem.Segment2, func) : 0.0;
                var CA = elem.Segment3.IsBoundary ? integrator.IntegrateOverLine(elem.Segment3, func) : 0.0;

                res[i] = coef * (AB + BC + CA);
            }

            return res;
        }

        private double[] GetVector6(FiniteElementTriangle elem)
        {
            var res = new double[3];

            for (int i = 0; i < 3; i++)
            {
                Func<double, double, double> func = (x, y) => elem.Nodes[i].LocalFunction.getValue(x, y) * problem.GivenFunc(x, y);

                res[i] = integrator.IntegrateOverTriangleSurface(elem, func);
            }

            return res;
        }
    }
}
