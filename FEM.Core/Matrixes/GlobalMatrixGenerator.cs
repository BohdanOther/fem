﻿using System.Collections.Generic;
using FEM.Core.FiniteElements;
using MathNet.Numerics.LinearAlgebra.Double;
using TriangleNet;

namespace FEM.Core.Matrixes
{
    public class GlobalMatrixGenerator
    {
        public double[,] GlobalMatrix;
        public double[] GlobalVector;

        private readonly LocalMatrixGenerator lgen;
        private readonly List<FiniteElementTriangle> elements;

        public GlobalMatrixGenerator(Mesh mesh, LocalMatrixGenerator lgen, List<FiniteElementTriangle> elements)
        {
            this.elements = elements;
            this.lgen = lgen;
            var size = mesh.Vertices.Count;

            GlobalMatrix = new double[size, size];
            GlobalVector = new double[size];

            FillMatrixes(mesh);
        }

        private void FillMatrixes(Mesh mesh)
        {

            foreach (var elem in elements)
            {
                lgen.CalcMatrixes(elem.Number);
                var m = lgen.LocalMatrix;
                var v = lgen.LocalVector;

                for (var i = 0; i < 3; i++)
                {
                    for (var j = 0; j < 3; j++)
                    {
                        GlobalMatrix[m[i, j].i, m[i, j].j] += m[i, j].Value;
                    }
                    GlobalVector[v[i].i] += v[i].Value;
                }
            }
        }

        public double[] Solve()
        {
            var A = Matrix.Build.DenseOfArray(GlobalMatrix);
            var b = Vector.Build.DenseOfArray(GlobalVector);

            var x = A.Solve(b);
            return x.ToArray();
        }
    }
}
