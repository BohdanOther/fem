﻿namespace FEM.Core.Matrixes
{
    public class LocalMatrix
    {
        public Item[,] Items;

        public struct Item
        {
            public double Value { get; private set; }
            public int i { get; private set; }
            public int j { get; private set; }

            public Item(double val, int i, int j)
            {
                this.Value = val;
                this.i = i;
                this.j = j;
            }
        }

        public LocalMatrix(int size)
        {
            Items = new Item[size, size];
        }

        [System.Runtime.CompilerServices.IndexerName("ItemIndexer")]
        public Item this[int i, int j]
        {
            get { return Items[i, j]; }
            set { Items[i, j] = value; }
        }
    }
}
