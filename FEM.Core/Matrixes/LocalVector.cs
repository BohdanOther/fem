﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FEM.Core.Matrixes
{
    public class LocalVector
    {
        public Item[] Items;

        public struct Item
        {
            public double Value { get; private set; }
            public int i { get; private set; }

            public Item(double val, int i)
            {
                this.Value = val;
                this.i = i;
            }
        }

        public LocalVector(int size)
        {
            Items = new Item[size];
        }

        [System.Runtime.CompilerServices.IndexerName("ItemIndexer")]
        public Item this[int i]
        {
            get { return Items[i]; }
            set { Items[i] = value; }
        }
    }
}
