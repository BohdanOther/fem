﻿using FEM.Core.FiniteElements;
using FEM.Core.Geometry;
using FEM.Core.Math;
using FEM.Core.Matrixes;
using System.Collections.Generic;
using System.Linq;

namespace FEM.Core
{
    public class FEMSolver
    {
        public List<FiniteElementTriangle> Elements;
        public List<FiniteElementPoint> Nodes;

        private FEMProblem problem;

        private readonly GaussianQuadratureIntegrator integrator;

        public FEMSolver()
        {
            integrator = new GaussianQuadratureIntegrator();
        }

        public FEMSolution Solve(FEMProblem problem)
        {
            this.problem = problem;
            Init();

            var lgen = new LocalMatrixGenerator(integrator, Elements, problem);
            var ggen = new GlobalMatrixGenerator(problem.Mesh, lgen, Elements);
            var u = ggen.Solve();

            return new FEMSolution(u, Nodes);
        }

        public void Init()
        {
            Elements = new List<FiniteElementTriangle>();
            Nodes = new List<FiniteElementPoint>();

            var boundarySegments = new List<LineSegment>();

            var boundaryPoints = problem.Mesh.Vertices.Where(v => v.Boundary == 1).Select(v => v.ToPoint()).ToArray();

            for (var i = 0; i < boundaryPoints.Length; i++)
            {
                var a = boundaryPoints[i];
                var b = i == boundaryPoints.Length - 1 ? boundaryPoints[0] : boundaryPoints[i + 1];

                var segment = new LineSegment(a.X, a.Y, b.X, b.Y);
                boundarySegments.Add(segment);
            }

            foreach (var tri in problem.Mesh.Triangles)
            {
                var elem = tri.ToFiniteElement();

                elem.Number = tri.ID;

                elem.Segment1.IsBoundary = boundarySegments.Find(e => e.Equals(elem.Segment1)) != null;
                elem.Segment2.IsBoundary = boundarySegments.Find(e => e.Equals(elem.Segment2)) != null;
                elem.Segment3.IsBoundary = boundarySegments.Find(e => e.Equals(elem.Segment3)) != null;

                Elements.Add(elem);
            }

            foreach (var pt in problem.Mesh.Vertices)
            {
                var num = pt.ID;
                var node = new FiniteElementPoint(pt.X, pt.Y, num);
                node.Initialize(Elements);
                Nodes.Add(node);
            }
        }
    }
}
