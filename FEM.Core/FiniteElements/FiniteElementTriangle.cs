﻿using System.Collections.Generic;
using FEM.Core.Geometry;

namespace FEM.Core.FiniteElements
{
    public class FiniteElementTriangle : Triangle
    {
        public int Number { get; set; }

        public LineSegment Segment1 { get; }
        public LineSegment Segment2 { get; }
        public LineSegment Segment3 { get; }

        public List<FiniteElementPoint> Nodes { get; } = new List<FiniteElementPoint>();

        public double Square => Utils.TriangleArea(this);

        public FiniteElementTriangle(Triangle t) : this(t[0], t[1], t[2]) { }

        public FiniteElementTriangle(Point point1, Point point2, Point point3)
            : base(point1, point2, point3)
        {
            Segment1 = new LineSegment(Points[0], Points[1]);
            Segment2 = new LineSegment(Points[1], Points[2]);
            Segment3 = new LineSegment(Points[2], Points[0]);
        }

        public bool containsPoint(Point pointToCheck)
        {
            //(x1-x0)*(y2-y1)-(x2-x1)*(y1-y0)
            //(x2-x0)*(y3-y2)-(x3-x2)*(y2-y0)
            //(x3-x0)*(y1-y3)-(x1-x3)*(y3-y0)
            var p1 = Points[0];
            var p2 = Points[1];
            var p3 = Points[2];

            double condition1 = (p1.X - pointToCheck.X) * (p2.Y - p1.Y) - (p2.X - p1.X) * (p1.Y - pointToCheck.Y);
            double condition2 = (p2.X - pointToCheck.X) * (p3.Y - p2.Y) - (p3.X - p2.X) * (p2.Y - pointToCheck.Y);
            double condition3 = (p3.X - pointToCheck.X) * (p1.Y - p3.Y) - (p1.X - p3.X) * (p3.Y - pointToCheck.Y);

            return ((condition1 >= 0 && condition2 >= 0 && condition3 >= 0) ||
                    (condition1 <= 0 && condition2 <= 0 && condition3 <= 0));
        }

        public LineSegment getRemoteSegment(Point point)
        {
            if (Points[0].Equals(point)) return Segment2;
            if (Points[1].Equals(point)) return Segment3;
            if (Points[2].Equals(point)) return Segment1;

            return new LineSegment();
        }

        public bool hasVertex(Point p)
        {
            return p.Equals(Points[0]) || p.Equals(Points[1]) || p.Equals(Points[2]);
        }
    }
}
