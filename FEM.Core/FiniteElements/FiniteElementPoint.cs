﻿using System.Collections.Generic;
using FEM.Core.Geometry;
using FEM.Core.Math;

namespace FEM.Core.FiniteElements
{
    public class FiniteElementPoint : Point
    {
        public int Number { get; set; }
        public LocalFunction LocalFunction { get; private set; }
        public List<FiniteElementTriangle> Elements;

        public FiniteElementPoint() { }
        public FiniteElementPoint(double x, double y) : base(x, y) { }
        public FiniteElementPoint(double x, double y, int number) : this(x, y) { Number = number; }

        public void Initialize(List<FiniteElementTriangle> allElements)
        {
            Elements = new List<FiniteElementTriangle>();

            foreach (FiniteElementTriangle triangle in allElements)
            {
                if (triangle.hasVertex(this))
                {
                    Elements.Add(triangle);
                    triangle.Nodes.Add(this);
                }
            }

            LocalFunction = new LocalFunction(Elements, this);
        }

    }
}
