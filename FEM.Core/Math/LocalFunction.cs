﻿using System.Collections.Generic;
using FEM.Core.FiniteElements;
using FEM.Core.Geometry;

namespace FEM.Core.Math
{
    public class LocalFunction
    {
        private FiniteElementPoint node;
        private List<FiniteElementTriangle> elements;

        public LocalFunction(List<FiniteElementTriangle> elements, FiniteElementPoint node)
        {
            this.elements = elements;
            this.node = node;
        }

        public double getValue(Point p)
        {
            foreach (var elementTriangle in elements)
            {
                if (elementTriangle.containsPoint(p))
                {
                    var s = elementTriangle.getRemoteSegment(node);
                    var partialTriangle = new FiniteElementTriangle(s.A, s.B, p);
                    return partialTriangle.Square / elementTriangle.Square;
                }
            }
            // point is outside
            return 0.0;
        }

        public double getValue(double x, double y)
        {
            return getValue(new Point(x, y));
        }
    }
}
