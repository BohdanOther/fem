﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FEM.Core.Geometry;

namespace FEM.Core.Math
{
    public class GaussianQuadratureIntegrator
    {
        #region Private fields
        private const int LEVEL = 2;
        private const double EPS = 0.01;

        private double error;

        private int level;
        private int stepNumberGaussian;

        private Func<double, double> f;
        private Func<double, double, double> f2D;
        #endregion

        public GaussianQuadratureIntegrator(double tollerance = EPS, int gaussianLevel = LEVEL)
        {
            error = tollerance;
            level = gaussianLevel;
        }

        public double Integrate(double left, double right, Func<double, double> f)
        {
            this.f = f;
            return ComputeGaussianValue(left, right);
        }
        public double Integrate2D(double a1, double b1, double a2, Func<double, double> b2, Func<double, double, double> f2d)
        {
            f2D = f2d;
            return ComputeGaussianValue2D(a1, b1, a2, b2);
        }

        public double IntegrateOverLine(LineSegment line, Func<double, double, double> f)
        {
            var res = 0.0;

            // integrate over dx if it's normal case
            if (!line.A.X.Equals(line.B.X))
            {
                var mline = line.ToCanonicX();
                var slope = (mline.B.Y - mline.A.Y) / (mline.B.X - mline.A.X);

                Func<double, double> fx = (x) =>
                     f(x, (mline.B.Y - mline.A.Y) * (x - mline.A.X) / (mline.B.X - mline.A.X) + mline.A.Y);

                var lineFuncDeriv = System.Math.Sqrt(1 + System.Math.Pow(slope, 2));

                var integralRes = Integrate(mline.A.X, mline.B.X, fx);

                res = lineFuncDeriv * integralRes;
            }
            else
            // integrate over dy, if it'a shitty case
            {
                var mline = line.ToCanonicY();
                var slope = (mline.B.X - mline.A.X) / (mline.B.Y - mline.A.Y);

                Func<double, double> fy = (y) =>
                     f((mline.B.X - mline.A.X) * (y - mline.A.Y) / (mline.B.Y - mline.A.Y) + mline.A.X, y);

                var lineFuncDeriv = System.Math.Sqrt(1 + System.Math.Pow(slope, 2));

                var integralRes = Integrate(mline.A.Y, mline.B.Y, fy);

                res = lineFuncDeriv * integralRes;
            }

            return res;
        }
        public double IntegrateOverTriangleContour(Triangle tr, Func<double, double, double> f)
        {
            var AB = new LineSegment(tr[0], tr[1]);
            var BC = new LineSegment(tr[1], tr[2]);
            var CA = new LineSegment(tr[2], tr[0]);
            
            // integral over trig countor = integral over each line in trig
            var res = IntegrateOverLine(AB, f) + IntegrateOverLine(BC, f) + IntegrateOverLine(CA, f);

            return res;
        }
        public double IntegrateOverTriangleSurface(Triangle tr, Func<double, double, double> f)
        {
            f2D = f;
            var a = tr.Points[0];
            var b = tr.Points[1];
            var c = tr.Points[2];

            // a lot of magic stuff
            // coordinate changes
            var jacobian = (b.X - a.X) * (c.Y - a.Y) - (c.X - a.X) * (b.Y - a.Y);
            // function rewriten in new coords
            Func<double, double, double> g = (u, v) => f(a.X + u * (b.X - a.X) + v * (c.X - a.X), a.Y + u * (b.Y - a.Y) + v * (c.Y - a.Y));
            // jacobian of new coord system
            var res = System.Math.Abs(jacobian) * Integrate2D(0.0, 1.0, 0.0, (u) => 1.0 - u, g);
            // here you have it
            return res;
        }

        #region 1D Integration

        private double ComputeGaussianValue(double leftSide, double rightSide)
        {
            stepNumberGaussian = 1;

            var resultAlghorithmValue = 0.0;
            var pointsGaussian = new List<double>();
            var needMoreAccurance = true;
            var h = (rightSide - leftSide) / stepNumberGaussian;

            for (var i = 0; i <= stepNumberGaussian; i++)
            {
                pointsGaussian.Add(leftSide + h * i);
            }

            for (var i = 0; i < stepNumberGaussian; i++)
            {
                var tempValue = GetQuadratureIntegral(pointsGaussian[i], pointsGaussian[i + 1]);
                resultAlghorithmValue += tempValue;
            }

            var previousValue = resultAlghorithmValue;

            stepNumberGaussian++;

            while (needMoreAccurance)
            {
                pointsGaussian.Clear();
                resultAlghorithmValue = 0;

                h = (rightSide - leftSide) / stepNumberGaussian;

                for (int i = 0; i <= stepNumberGaussian; i++)
                {
                    pointsGaussian.Add(leftSide + h * i);
                }

                for (int i = 0; i < stepNumberGaussian; i++)
                {
                    var tempValue = GetQuadratureIntegral(pointsGaussian[i], pointsGaussian[i + 1]);
                    resultAlghorithmValue += tempValue;
                }

                if (System.Math.Abs(resultAlghorithmValue - previousValue) < error)
                {
                    needMoreAccurance = false;
                    stepNumberGaussian--;
                    stepNumberGaussian *= level;
                    resultAlghorithmValue = previousValue;
                }
                else
                {
                    stepNumberGaussian++;
                    previousValue = resultAlghorithmValue;
                }
            }

            return resultAlghorithmValue;
        }

        private double GetQuadratureIntegral(double a, double b)
        {
            var result = 0.0;

            for (var i = 0; i < level; i++)
            {
                var nodeExact = a + (b - a) * (nodes[level - 2][i] + 1) / 2.0;
                result += f(nodeExact) * weights[level - 2][i];
            }

            return (b - a) * result / 2;

        }

        #endregion

        #region 2D Integration

        private double ComputeGaussianValue2D(double a1, double b1, double a2, Func<double, double> upperBound)
        {
            stepNumberGaussian = 1;

            var resultAlghorithmValue = 0.0;
            var pointsGaussian = new List<double>();
            var needMoreAccurance = true;

            var h = (b1 - a1) / stepNumberGaussian;
            for (var i = 0; i <= stepNumberGaussian; i++)
            {
                pointsGaussian.Add(a1 + h * i);
            }

            for (var i = 0; i < stepNumberGaussian; i++)
            {
                var tempValue = GetQuadratureIntegral2D(pointsGaussian[i], pointsGaussian[i + 1], a2, upperBound((pointsGaussian[i + 1] + pointsGaussian[i]) / 2));
                resultAlghorithmValue += tempValue;
            }

            var previousValue = resultAlghorithmValue;

            stepNumberGaussian++;

            while (needMoreAccurance)
            {
                pointsGaussian.Clear();
                resultAlghorithmValue = 0;

                h = (b1 - a1) / stepNumberGaussian;

                for (int i = 0; i <= stepNumberGaussian; i++)
                {
                    pointsGaussian.Add(a1 + h * i);
                }

                for (int i = 0; i < stepNumberGaussian; i++)
                {
                    var tempValue = GetQuadratureIntegral2D(pointsGaussian[i], pointsGaussian[i + 1], a2, upperBound((pointsGaussian[i + 1] + pointsGaussian[i]) / 2));
                    resultAlghorithmValue += tempValue;
                }

                if (System.Math.Abs(resultAlghorithmValue - previousValue) < error)
                {
                    needMoreAccurance = false;
                    stepNumberGaussian--;
                    stepNumberGaussian *= level;
                    resultAlghorithmValue = previousValue;
                }
                else
                {
                    stepNumberGaussian++;
                    previousValue = resultAlghorithmValue;
                }
            }

            return resultAlghorithmValue;
        }

        private double GetQuadratureIntegral2D(double a_outer, double b_outer, double a_inner, double b_inner)
        {
            var result = 0.0;

            for (var i = 0; i < level; i++)
            {
                var nodeExact = a_outer + (b_outer - a_outer) * (nodes[level - 2][i] + 1) / 2.0;
                result += QuadratureIntegral2dHelper(nodeExact, a_inner, b_inner) * weights[level - 2][i];
            }

            return (b_outer - a_outer) * result / 2;
        }

        private double QuadratureIntegral2dHelper(double x, double a, double b)
        {
            var result = 0.0;

            for (var i = 0; i < level; i++)
            {
                var y = a + (b - a) * (nodes[level - 2][i] + 1) / 2.0;
                result += f2D(x, y) * weights[level - 2][i];
            }

            return (b - a) * result / 2;
        }

        #endregion

        #region Gauss constans

        private double[][] nodes = {
            new double[] {-0.577350, 0.577350},
            new double[] {-0.774596, 0.000000, 0.774596},
            new double[] {-0.861136, -0.339981, 0.339981, 0.861136},
            new double[] {-0.906179, -0.538469, 0.000000, 0.538469, 0.906179},
            new double[] {-0.932469, -0.661209, -0.238619, 0.238619, 0.661209, 0.932469},
            new double[] {-0.949107, -0.741531, -0.405845, 0.000000, 0.405845, 0.741531, 0.9491079},
            new double[] {-0.960289, -0.796666, -0.525532, -0.183434, 0.183434, 0.525532, 0.796666, 0.960289}
        };

        private static double[][] weights = {
            new double[] {1.000000, 1.000000},
            new double[] {0.555555, 0.888888, 0.555555},
            new double[] {0.347854, 0.652145, 0.652145, 0.347854},
            new double[] {0.236926, 0.478628, 0.568888, 0.478628, 0.236926},
            new double[] {0.171324, 0.360761, 0.467913, 0.467913, 0.360761, 0.171324},
            new double[] {0.129484, 0.279705, 0.381830, 0.417959, 0.381830, 0.279705, 0.129484},
            new double[] {0.101228, 0.222381, 0.313706, 0.362683, 0.362683, 0.313706, 0.222381, 0.101228}
        };

        #endregion
    }
}
