﻿using System;
using FEM.Common.Functions;
using TriangleNet;
using TriangleNet.Geometry;

namespace FEM.Core
{
    public class FEMProblem
    {
        public Mesh Mesh;

        public Func<double, double, double> EnvironmentFunc;
        public Func<double, double, double> GivenFunc;

        public double C;
        public double Alpha;
        public double Beta;

        /// <summary>
        /// Basic heat problem
        /// on a quad area of 2 finite triangle elements
        /// </summary>
        public static FEMProblem TestProblem
        {
            get
            {
                var input = new InputGeometry();
                input.AddPoint(0, 0);
                input.AddPoint(2, 0);
                input.AddPoint(2, 1);
                input.AddPoint(0, 1);
                var mesh = new Mesh();
                mesh.Triangulate(input);

                return new FEMProblem
                {
                    Mesh = mesh,
                    EnvironmentFunc = (x, y) => x * y + 200,
                    GivenFunc = (x, y) => 0,
                    C = 0,
                    Alpha = 6,
                    Beta = 5
                };
            }
        }
    }
}
