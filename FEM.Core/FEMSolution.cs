﻿using FEM.Core.FiniteElements;
using System;
using System.Collections.Generic;

namespace FEM.Core
{
    public class FEMSolution
    {
        private readonly double[] _u;
        private readonly List<FiniteElementPoint> _nodes;

        public FEMSolution(double[] u, List<FiniteElementPoint> nodes)
        {
            _u = u;
            _nodes = nodes;

            if (u.Length != nodes.Count) throw new Exception("Something gone wrong");
        }

        public double GetValue(double x, double y)
        {
            var res = 0.0;

            // ReSharper disable once LoopCanBeConvertedToQuery
            for (var i = 0; i < _nodes.Count; i++)
            {
                res += _u[i] * _nodes[i].LocalFunction.getValue(x, y);
            }

            return res;
        }
    }
}
