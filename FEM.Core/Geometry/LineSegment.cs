﻿using System;

namespace FEM.Core.Geometry
{
    public class LineSegment
    {
        public Point A { get; set; } = Point.Empty;
        public Point B { get; set; } = Point.Empty;

        public bool IsBoundary { get; set; }

        public LineSegment() { }
        public LineSegment(Point a, Point b) { A = a; B = b; }
        public LineSegment(double x1, double y1, double x2, double y2) : this(new Point(x1, y1), new Point(x2, y2)) { }

        /// <summary>
        /// Returns line where a.x less then b.x
        /// </summary>
        public LineSegment ToCanonicX()
        {
            return A.X <= B.X ? new LineSegment(A, B) : new LineSegment(B, A);
        }

        /// <summary>
        /// Returns line where a.x less then b.x
        /// </summary>
        public LineSegment ToCanonicY()
        {
            return A.Y <= B.Y ? new LineSegment(A, B) : new LineSegment(B, A);
        }

        public bool Equals(LineSegment line)
        {
            var a = this.ToCanonicX();
            var b = line.ToCanonicX();
            return a.A.Equals(b.A) && a.B.Equals(b.B);
        }
    }
}
