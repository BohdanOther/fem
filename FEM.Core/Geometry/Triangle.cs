﻿using System.Collections;
using System.Collections.Generic;

namespace FEM.Core.Geometry
{
    public class Triangle : IEnumerable<Point>
    {
        private Point[] points = { Point.Empty, Point.Empty, Point.Empty };

        public List<Point> Points => new List<Point>(points);

        public Triangle() { }

        public Triangle(Point pt1, Point pt2, Point pt3)
        {
            points = new[] { pt1, pt2, pt3 };

            Points[0] = pt1;

            if (Utils.wedgeProduct(pt1, pt2, pt1, pt3) >= 0)
            {
                Points[1] = pt2;
                Points[2] = pt3;
            }
            else
            {
                Points[1] = pt3;
                Points[2] = pt2;
            }
        }

        #region IEnumerable

        public Point this[int index]
        {
            get { return ((index > -1) && (index < 3)) ? points[index] : Point.Empty; }
            set { if ((index > -1) && (index < 3)) points[index] = value; }
        }

        public IEnumerator<Point> GetEnumerator()
        {
            foreach (var p in points)
            {
                yield return p;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion
    }
}
