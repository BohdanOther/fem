﻿namespace FEM.Core.Geometry
{
    public class Point
    {
        public double X { get; set; }
        public double Y { get; set; }

        public Point() { }
        public Point(double x, double y) { X = x; Y = y; }

        public static Point Empty => new Point();

        public bool Equals(Point p)
        {
            return X.Equals(p.X) && Y.Equals(p.Y);
        }
    }
}
