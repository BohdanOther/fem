﻿using NCalc;

namespace FEM.Common.Functions
{
    public class SymbolicFunction : IFunction
    {
        private readonly int _dim;
        private readonly Expression _expr;
        private readonly string[] _args;

        /// <summary>
        /// Multivariable scalar symbolic function evaluator
        /// For dim = n will be x1, x2, ..., xn arguments 
        /// </summary>
        /// <param name="expr">Symbolic expressions to evaluate</param>
        /// <param name="dim">Number of variables</param>
        /// <param name="arg">Name of variable. By default 'x'</param>
        public SymbolicFunction(string expr, int dim, string arg = "x")
        {
            _dim = dim > 0 ? dim : 1;

            _expr = new Expression(expr);

            _args = new string[dim];
            for (var i = 1; i <= dim; i++)
            {
                _args[i - 1] = $"{arg}{i}";
            }
        }

        /// <summary>
        /// z = f(x, y)
        /// </summary>
        public SymbolicFunction(string expr)
        {
            _dim = 2;

            _expr = new Expression(expr);

            _args = new[] { "x", "y" };
        }

        public double Val(double x, double y)
        {
            _expr.Parameters[_args[0]] = x;
            _expr.Parameters[_args[1]] = y;

            var res = (double)_expr.Evaluate();

            return res;
        }
    }
}
