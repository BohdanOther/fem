﻿namespace FEM.Common.Functions
{
    public interface IFunction
    {
        double Val(double x, double y);
    }
}
