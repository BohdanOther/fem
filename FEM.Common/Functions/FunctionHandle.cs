﻿using System;

namespace FEM.Common.Functions
{
    public class FunctionHandle : IFunction
    {
        private readonly Func<double, double, double> _f;

        public FunctionHandle(Func<double, double, double> f)
        {
            _f = f;
        }

        public double Val(double x, double y)
        {
            return _f(x, y);
        }
    }
}
